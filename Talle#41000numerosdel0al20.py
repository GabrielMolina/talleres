#Dado un conjunto de valores en una lista de 1000 valores generados aleatoriamente
#entre 0 y 20

#1. Crear otra lista con valor True o False dependiendo si el valor de la lista original es primo o no
#2. Crear una funcion que devuelva la media de la lista generada
#3. Crear una funcion que devuelva una lista con el factorial de cada uno de los numeros de la lista
#4. Crear una funcion que retorne el valor mayor y el menor de la lista

import random as rd
import statistics as st
import math


n=[]
for _ in range(1000):
    j=rd.randint(0,20)
    n.append(j)
print("Los 1000 numeros aleatorios son:")
print(n)

def primo(n):
    if n>1:
        for i in range(2,n):
            if (n%i)==0:
                return False
    else:
        return False
    return True
p=[primo(i) for i in n]
print("Los numeros que son primos son:")
print(p)

def media(n):
    media=st.mean(n)
    return media
print("La media es: ", media(n))

def factorial(n):
    f=[] 
    for s in n:
        m=math.factorial(s)
        f.append(m)
    return f  
print("Los factoriales de los numero son: ")
print(factorial(n))

def mayormenor(n):
    mayor=max(n)
    menor=min(n)
    return mayor,menor
print("El mayor y el menor numero obteido es: ", mayormenor(n))







  
