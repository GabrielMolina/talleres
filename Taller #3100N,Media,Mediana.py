#Importando y utilizando la biblioteca pandas, 
#Calcular la mediana y la media de los datos
#almacenados en una lista de 100 elementos.
#La lista debe "llenarse" con numeros aleaterios de -100 a 100.
import pandas as pd
import numpy as np
import math as mt


numero=[]

for _ in range(100):
    j=np.random.randint(-100,100)
    numero.append(j)

jp=pd.DataFrame([numero])
mediana=np.float64(jp.median(axis=1)) 
media= np.float64(jp.mean(axis=1))
moda=np.float64(jp.mode(axis=1))
print("Los 100 numeros aleatorios son:")
print(numero)
print("La mediana es: ",mediana)
print("La media es: ", media)
print("La moda es: ", moda)